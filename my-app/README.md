# Innoloft User Dashboard

## Run live

This is project is deployed on [Netlify](https://eloquent-aryabhata-f6b832.netlify.com): https://eloquent-aryabhata-f6b832.netlify.com/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements

In first tab (basic data)

- [x] Change e-mail address
Change Password
The password must have certain properties:
- [x] "Password" and "Password repeat" fields need to be identical (including an indicator for this equality)
The password field should accept Uppercase letters, lowercase letters, numbers and special characters
A multi-color password strength indicator should be implemented
- [x] Button to update the user data

In the second tab (Address):

- [x] Change first name
- [x] Change Last Name
- [x] Change address (street, house number, postal code)
- [x] Change country (Germany, Austria, Switzerland are available)
- [x] Button to update the user data

The application should at the very least use the following:

- [x] Do not use Bootsrap or similar frameworks for CSS, just create new CSS from scratch.
- [x] React.js framework
- [x] Use Redux for state management
- [x] A CSS pre-compiler (SASS, LESS, SCSS) or other CSS approaches (CSS modules, Styled components)

## Deadline

- [x] 3 days about 5 hours


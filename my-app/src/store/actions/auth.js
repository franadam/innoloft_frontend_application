import axios from 'axios';

import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAIL,
  AUTH_LOGOUT,
  AUTH_SIGN_UP,
  SET_AUTH_REDIRECT_PATH,
} from './types';

const authStart = () => {
  return {
    type: AUTH_START,
  };
};

const authSuccess = (userID) => {
  return {
    type: AUTH_SUCCESS,
    userID,
  };
};

const authSignUp = () => {
  return {
    type: AUTH_SIGN_UP,
  };
};

export const setAuthRedirectPath = (path) => {
  return {
    type: SET_AUTH_REDIRECT_PATH,
    path: path,
  };
};

export const authLogout = () => {
  return {
    type: AUTH_LOGOUT,
  };
};

export const authFail = (error) => {
  return {
    type: AUTH_FAIL,
    error: error,
  };
};

export const signin = ({ email, password }) => {
  return async (dispatch) => {
    dispatch(authStart());

    try {
      const res = await axios.post(
        `https://my-form.free.beeceptor.com/api/login`,
        {
          email,
          password,
        }
      );
      const { user } = res.data;
      dispatch(authSuccess(user._id));
    } catch (error) {
      dispatch(
        authFail({
          ...error,
        })
      );
    }
  };
};

export const signup = (data) => {
  return async (dispatch) => {
    dispatch(authStart());

    try {
      const res = await axios.post(
        `https://my-form.free.beeceptor.com/api/signup`,
        data
      );
      const { status } = res.data;
      dispatch(authSignUp(!!status));
    } catch (error) {
      dispatch(
        authFail({
          ...error,
        })
      );
    }
  };
};

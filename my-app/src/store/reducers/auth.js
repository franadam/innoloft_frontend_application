import { updateObject } from '../../shared/utils';

import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAIL,
  AUTH_LOGOUT,
  AUTH_SIGN_UP,
  SET_AUTH_REDIRECT_PATH,
} from '../actions/types';

const initialState = {
  userID: null,
  loading: false,
  authRedirectPath: '/',
  error: null,
  isSignedUp: false,
};

const authStart = (state) => {
  return updateObject(state, {
    error: null,
    loading: true,
  });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    userID: action.userID,
    loading: false,
    error: null,
  });
};

const authLogout = (state) => {
  return updateObject(state, {
    userID: null,
  });
};

const authSignUp = (state) => {
  return updateObject(state, {
    isSignedUp: true,
  });
};

const authFail = (state, action) => {
  return updateObject(state, { error: action.error, loading: false });
};

const setAuthRedirectPath = (state, action) => {
  return updateObject(state, { authRedirectPath: action.path });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_START:
      return authStart(state);
    case AUTH_FAIL:
      return authFail(state, action);
    case AUTH_SUCCESS:
      return authSuccess(state, action);
    case AUTH_LOGOUT:
      return authLogout(state);
    case AUTH_SIGN_UP:
      return authSignUp(state);
    case SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, action);
    default:
      return state;
  }
};

export default reducer;

import validator from 'validator';

export const validate = (element) => {
  let error = [true, ''];

  if (element.validation.email) {
    const valid = validator.isEmail(element.value);
    const message = `${!valid ? 'Please enter a valid email' : ''}`;
    error = !valid ? [valid, message] : error;
  }

  if (element.validation.required) {
    const valid = element.value.trim() !== '';
    const message = `${
      !valid ? `The ${element.config.name} field is required` : ''
    }`;
    error = !valid ? [valid, message] : error;
  }

  return error;
};

export const showPassword = () => {
  const x = document.getElementById('password');
  if (x.type === 'password') {
    x.type = 'text';
  } else {
    x.type = 'password';
  }
};

export const updateObject = (oldObj, updatedProperties) => {
  return {
    ...oldObj,
    ...updatedProperties,
  };
};

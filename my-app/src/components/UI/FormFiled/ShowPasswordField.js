import React from 'react';
import { showPassword } from '../../../shared/utils';
import classes from './FormField.module.scss';

const showPasswordField = () => {
  return (
    <div className={classes.show_password}>
      <label
        className={classes.show_password_label}
        htmlFor=" "
        id="checklabel"
      >
        <input id="checkbox" onClick={showPassword} type="checkbox" /> <span />
        Show Password
      </label>
    </div>
  );
};

export default showPasswordField;

import React from 'react';
import PropTypes from 'prop-types';

const AdditionalForm = ({ createForm, formData, currentStep }) => {
  if (currentStep !== 2) {
    return null;
  }

  return <>{createForm(formData)}</>;
};

AdditionalForm.propTypes = {
  currentStep: PropTypes.number,
  formData: PropTypes.object,
  createForm: PropTypes.func,
};

export default AdditionalForm;

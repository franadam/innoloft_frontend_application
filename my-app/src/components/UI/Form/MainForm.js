import React from 'react';
import PropTypes from 'prop-types';
import ShowPasswordField from '../FormFiled/ShowPasswordField';

const MainForm = ({ createForm, currentStep, formData, score }) => {
  if (currentStep !== 1) {
    return null;
  }

  return (
    <>
      {createForm(formData)}

      <ShowPasswordField />

      <div>
        <div>
          <label htmlFor="progress">Password strength</label>
        </div>
        <div>
          <div className="progress" id="progress">
            <div
              className="progress__bar"
              id="bar"
              style={{ height: '24px', width: `${score}%` }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

MainForm.propTypes = {
  currentStep: PropTypes.number,
  score: PropTypes.number,
  formData: PropTypes.object,
  createForm: PropTypes.func,
};

export default MainForm;

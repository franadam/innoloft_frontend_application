import React from 'react';
import PropTypes from 'prop-types';

const ConfirmationForm = ({ createForm, currentStep, formData }) => {
  if (currentStep !== 3) {
    return null;
  }

  return (
    <>{createForm(formData).filter((f) => f.key !== 'password_confirmation')}</>
  );
};

ConfirmationForm.propTypes = {
  currentStep: PropTypes.number,
  formData: PropTypes.object,
  createForm: PropTypes.func,
};

export default ConfirmationForm;

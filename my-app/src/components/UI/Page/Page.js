import React from 'react';
import PropTypes from 'prop-types';
import LayoutAdmin from '../../../hoc/LayoutAdmin/LayoutAdmin';

import classes from './Page.module.scss';

const Page = ({ title }) => {
  return (
    <LayoutAdmin>
      <div className={classes.main}>
        <h1>{title}</h1>
      </div>
    </LayoutAdmin>
  );
};

Page.propTypes = {
  title: PropTypes.string,
};

export default Page;

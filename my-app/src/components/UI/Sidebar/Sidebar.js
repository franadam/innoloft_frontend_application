import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  FaHome,
  FaUser,
  FaCog,
  FaBuilding,
  FaEdit,
  FaChartArea,
  FaDoorOpen,
} from 'react-icons/fa';

import { authLogout } from '../../../store/actions/auth';

import classes from './Sidebar.module.scss';

const Sidebar = (props) => {
  return (
    <ul className={classes.main}>
      <li>
        <Link to="/home">
          <FaHome />
          <div className={classes.link}>Home</div>
        </Link>
      </li>
      <li>
        <Link to="/account">
          <FaUser />
          <div className={classes.link}>My Account</div>
        </Link>
      </li>
      <li>
        <Link to="/company">
          <FaBuilding />
          <div className={classes.link}>My Company</div>
        </Link>
      </li>
      <li>
        <Link to="/setting">
          <FaCog />
          <div className={classes.link}>My Settings</div>
        </Link>
      </li>
      <li>
        <Link to="/news">
          <FaEdit />
          <div className={classes.link}>News</div>
        </Link>
      </li>
      <li>
        <Link to="/analytics">
          <FaChartArea />
          <div className={classes.link}>Analytics</div>
        </Link>
      </li>
      <li>
        <div className={classes.logout} onClick={() => props.onAuthLogout()}>
          <FaDoorOpen />
          <div className={classes.link}>Logout</div>
        </div>
      </li>
    </ul>
  );
};

const mapDispatchToProps = (dispatch) => ({
  onAuthLogout: () => dispatch(authLogout()),
});

Sidebar.propTypes = {
  onAuthLogout: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(Sidebar);

import React from 'react';
import LayoutAdmin from '../../../hoc/LayoutAdmin/LayoutAdmin';

import classes from './Dashboard.module.scss';
const Dashboard = () => {
  return (
    <LayoutAdmin>
      <div className={classes.main}>
        <h1>Welcome</h1>
        This is your Dashboard !
      </div>
    </LayoutAdmin>
  );
};

export default Dashboard;

import React from 'react';
import { Link } from 'react-router-dom';
import { FaBars, FaCaretDown, FaGlobeEurope } from 'react-icons/fa';

import classes from './Header.module.scss';
import './Header.module.scss';
const Header = () => {
  const logo = (
    <a
      href="https://innoloft.com/public/en/"
      rel="noopener noreferrer"
      target="_blank"
    >
      <img
        alt="logo-innoloft"
        className={classes.logo}
        src="https://innoloft.com/public/wp-content/uploads/2019/09/logo_innoloft.png"
      />
    </a>
  );

  const dropdownHandler = (event) => {
    const btn = event.currentTarget;
    const content = btn.parentElement.querySelector(
      '.dropdown__btn ~ .dropdown__content'
    );
    if (btn.style.display === 'block') {
      content.style.display = 'none';
    } else {
      content.style.display = 'block';
    }
  };

  const mainLinks = (
    <div className={classes.links}>
      <Link className={classes.mylink} to="/how-it-works">
        How it works
      </Link>

      <div className="dropdown">
        <button
          className="dropdown__btn"
          onClick={(event) => dropdownHandler(event)}
        >
          Network
          <FaCaretDown />
        </button>
        <div className="dropdown__content">
          <Link className={classes.mylink} to="/network-startups">
            Startups
          </Link>
          <Link className={classes.mylink} to="/network-requests">
            Requests
          </Link>
          <Link className={classes.mylink} to="/network-news">
            News
          </Link>
          <Link className={classes.mylink} to="/network-challenges">
            Challenges
          </Link>
          <Link className={classes.mylink} to="/network-speed-dating">
            Speed Dating
          </Link>
        </div>
      </div>
      <div className="dropdown">
        <button
          className="dropdown__btn"
          onClick={(event) => dropdownHandler(event)}
        >
          Product
          <FaCaretDown />
        </button>
        <div className="dropdown__content">
          <Link className={classes.mylink} to="/use-case">
            Use Case
          </Link>
          <Link className={classes.mylink} to="/innoloft-pro">
            Innoloft Pro
          </Link>
          <Link className={classes.mylink} to="/challenge">
            Challenge
          </Link>
          <Link className={classes.mylink} to="/innovation-consulting">
            Innovation Consulting
          </Link>
          <Link className={classes.mylink} to="/platform">
            Platform
          </Link>
          <Link className={classes.mylink} to="/startups-speed-dating">
            Startups Speed Dating
          </Link>
        </div>
      </div>
      <div className="dropdown">
        <button
          className="dropdown__btn"
          onClick={(event) => dropdownHandler(event)}
        >
          Innoloft
          <FaCaretDown />
        </button>
        <div className="dropdown__content">
          <Link className={classes.mylink} to="/press">
            Press
          </Link>
          <Link className={classes.mylink} to="/about-innolof">
            Vision
          </Link>
          <Link className={classes.mylink} to="/about-innolof/#team">
            Team
          </Link>
          <Link className={classes.mylink} to="/about-innolof/#career">
            Career
          </Link>
        </div>
      </div>

      <Link className={classes.mylink} to="/blog">
        Blog
      </Link>
    </div>
  );
  const registerLink = (
    <div className={classes.register}>
      <Link className={classes.mylink} to="/sign-in">
        Login
      </Link>
      <Link className="btn--register" to="/sign-in">
        Register for free
      </Link>
      <div className="dropdown">
        <button
          className="dropdown__btn"
          onClick={(event) => dropdownHandler(event)}
        >
          <FaGlobeEurope />
          EN
          <FaCaretDown />
        </button>
        <div className={`${classes.languages} dropdown__content`}>
          <Link className={classes.mylink} to="/sign-in">
            EN
          </Link>
          <Link className={classes.mylink} to="/sign-in">
            DE
          </Link>
        </div>
      </div>
    </div>
  );
  const toggleNavbar = () => {
    const links = document.querySelector(`.${classes.links}`);
    const register = document.querySelector(`.${classes.register}`);
    if (links.style.display === 'block') {
      links.style.display = 'none';
      register.style.display = 'none';
    } else {
      links.style.display = 'block';
      register.style.display = 'block';
    }
  };
  const menu = (
    <div className={classes.menu} onClick={toggleNavbar}>
      <FaBars size="2.4rem" />
    </div>
  );

  return (
    <nav className={classes.main}>
      <div className={classes.wrapper}>
        <div className={classes.toolbar}>
          {logo}
          {menu}
        </div>
        {mainLinks}
        {registerLink}
      </div>
    </nav>
  );
};

export default Header;

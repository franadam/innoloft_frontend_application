import React from 'react';
import { Link } from 'react-router-dom';
import {
  FaLinkedin,
  FaTwitter,
  FaFacebookSquare,
  FaInstagram,
  FaYoutube,
  FaPenNib,
} from 'react-icons/fa';
import classes from './Footer.module.scss';

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <a
        href="https://innoloft.com/public/en/"
        rel="noopener noreferrer"
        target="_blank"
      >
        <img
          className={classes.logo}
          alt="logo"
          src="https://innoloft.com/public/wp-content/uploads/2019/09/logo_innoloft_white.png"
        />
      </a>
      <div className={classes.links}>
        <div className="social">
          <h3>Social</h3>
          <ul>
            <li>
              <a
                href="https://www.linkedin.com/company/innoloft"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaLinkedin /> Linkedin
              </a>
            </li>
            <li>
              <a
                href="https://twitter.com/innoloft_net"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaTwitter /> Twitter
              </a>
            </li>
            <li>
              <a
                href="https://www.facebook.com/InnoloftNetwork"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaFacebookSquare /> Facebook
              </a>
            </li>
            <li>
              <a
                href="https://www.instagram.com/innoloft/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaInstagram /> Instagram
              </a>
            </li>
            <li>
              <a
                href="https://www.youtube.com/channel/UCVjfGwUykde8cuCl31CX8Nw"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaYoutube /> Youtube
              </a>
            </li>
            <li>
              <a
                href="https://innoloft.com/public/en/blog-en/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <FaPenNib /> Blog
              </a>
            </li>
          </ul>
        </div>
        <div className="legal">
          <h3>Legal matters</h3>
          <ul>
            <li>
              <a href="https://innoloft.com/public/en/abg-2">Terms of Usage</a>
            </li>
            <li>
              <a href="https://innoloft.com/public/en/data-privacy-en">
                Privacy Policies
              </a>
            </li>
            <li>
              <a href="https://innoloft.com/public/en/imprint-en">
                Imprint and Legal Notice
              </a>
            </li>
          </ul>
        </div>
        <div className="info contact">
          <h3>Contact</h3>
          <p>
            Innoloft GmbHp <br /> c/o DigitalHUB Aache
            <br /> Jülicher Straße 72a
            <br /> 52070 Aachen
            <br />
            <a href="https://innoloft.com/public/en/">info@innoloft.com</a>
          </p>
        </div>
        <div className="info login">
          <Link to="/sign-in">
            <h3>Login</h3>
          </Link>
          <Link className={`${classes.btn} btn--register`} to="/sign-in">
            Register for free
          </Link>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

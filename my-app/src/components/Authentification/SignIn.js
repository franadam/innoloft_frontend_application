import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import FormField from '../UI/FormFiled/FormField';
import ShowPasswordField from '../UI/FormFiled/ShowPasswordField';

import { validate } from '../../shared/utils';

import classes from './Authentification.module.scss';
import { signin, setAuthRedirectPath } from '../../store/actions/auth';

class SignIn extends Component {
  state = {
    formError: false,
    formSuccess: '',
    formData: {
      email: {
        element: 'input',
        value: '',
        config: {
          name: 'email',
          type: 'email',
          placeholder: 'Enter your email',
        },
        validation: {
          required: true,
          email: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      password: {
        element: 'input',
        value: '',
        config: {
          name: 'password',
          type: 'password',
          placeholder: 'Enter your password',
        },
        validation: {
          required: true,
          minLength: 8,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
    },
  };

  createForm = (formData) => {
    const formElementsArray = [];
    for (let key in formData) {
      formElementsArray.push({
        id: key,
        config: formData[key],
      });
    }

    const formularField = formElementsArray.map((elem) => (
      <React.Fragment key={elem.id}>
        <label htmlFor={elem.id} className={classes.label}>
          {elem.id.replace('_', ' ')}
        </label>
        <FormField
          id={elem.id}
          field={elem.config}
          change={(event) => this.formFieldHandler(event)}
        />
      </React.Fragment>
    ));

    return formularField;
  };

  formFieldHandler = (element) => {
    const newFormData = { ...this.state.formData };
    const newElement = { ...newFormData[element.id] };

    newElement.value = element.event.target.value;

    const [valid, validationMessage] = validate(newElement);
    newElement.valid = valid;
    newElement.validationMessage = validationMessage;

    newFormData[element.id] = newElement;

    this.setState({
      formData: newFormData,
      formError: false,
    });
  };

  formSuccesManager = (type) => {
    const newFormData = { ...this.state.formData };

    for (let key in newFormData) {
      newFormData[key].value = '';
      newFormData[key].valid = false;
      newFormData[key].validationMessage = '';
    }

    this.clearSuccesMessage();

    this.setState({
      formData: newFormData,
      formError: false,
      formSuccess: type ? 'Congratulation' : 'This user already exists',
    });
  };

  clearSuccesMessage = () => {
    setTimeout(() => {
      this.setState({ formSuccess: '' });
    }, 2000);
  };

  formHandler = (event) => {
    event.preventDefault();

    const dataToSubmit = {};
    let isValid = true;

    for (let key in this.state.formData) {
      dataToSubmit[key] = this.state.formData[key].value;
      isValid = isValid && this.state.formData[key].valid;
    }

    if (isValid) {
      this.props.onSignin({
        email: dataToSubmit.email,
        password: dataToSubmit.password,
      });
      this.props.onSetAuthRedirectPath('/dashboard');
      this.props.history.push('/dashboard');
    } else {
      this.setState({ formError: true });
    }
  };

  render() {
    let formErrorMessage = (
      <div className={classes.error}>
        {this.state.formError
          ? `${this.props.error.message} Someting went wrong`
          : null}
      </div>
    );

    let errorMessage = (
      <div className={classes.error}>
        {!this.props.isAuthenticated && this.props.error
          ? `${this.props.error.message}`
          : null}
      </div>
    );

    let form = (
      <form className={classes.form} onSubmit={(event) => {}}>
        {this.createForm(this.state.formData)}
        <div></div>
        <ShowPasswordField />

        <button
          className={`${classes.btn} ${classes.btnSwitch}`}
          onClick={() => this.props.history.push('/sign-up')}
        >
          SWITCH TO SIGN UP
        </button>
        <button
          type="submit"
          className={classes.btn}
          onClick={(event) => this.formHandler(event)}
        >
          LOG IN
        </button>
        {formErrorMessage}
        <div className={classes.success_label}>{this.state.formSuccess}</div>
      </form>
    );

    return (
      <div className={classes.main}>
        <h2 className={classes.title}>Please SIGN IN</h2>
        <div className={classes.wrapper}>
          {form}
          {errorMessage}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
    isAuthenticated: !!state.userID,
    authRedirectPath: state.authRedirectPath,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onSignin: (credential) => dispatch(signin(credential)),
  onSetAuthRedirectPath: (path) => dispatch(setAuthRedirectPath(path)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SignIn));

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { FaCheck, FaTimes } from 'react-icons/fa';

import MainForm from '../UI/Form/MainForm';
import AdditionalForm from '../UI/Form/AdditionalForm';
import FormField from '../UI/FormFiled/FormField';
import ConfirmationForm from '../UI/Form/ConfirmationForm';

import { validate } from '../../shared/utils';
import { signup, setAuthRedirectPath } from '../../store/actions/auth';

import classes from './Authentification.module.scss';
import styleForm from '../UI/FormFiled/FormField.module.scss';

class SignUp extends Component {
  state = {
    currentStep: 1,
    formError: false,
    formSuccess: '',
    formValid: false,
    formData: {
      email: {
        element: 'input',
        value: '',
        config: {
          name: 'email',
          type: 'email',
          placeholder: 'Enter your email',
        },
        validation: {
          required: true,
          email: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      password: {
        element: 'input',
        value: '',
        config: {
          name: 'password',
          type: 'password',
          placeholder: 'Enter your password',
        },
        validation: {
          required: true,
          minLength: false,
          hasUpperCase: false,
          hasLowerCase: false,
          hasSpecialChar: false,
          hasNumber: false,
        },
        score: 0,
        valid: false,
        validationMessage: '',
        touched: false,
      },
      password_confirmation: {
        element: 'input',
        value: '',
        config: {
          name: 'password_confirmation',
          type: 'password',
          placeholder: 'Enter your password',
        },
        validation: {
          required: true,
          minLength: 8,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      firstname: {
        element: 'input',
        value: '',
        config: {
          name: 'firstname',
          type: 'text',
          placeholder: 'Enter your firstname',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      lastname: {
        element: 'input',
        value: '',
        config: {
          name: 'lastname',
          type: 'text',
          placeholder: 'Enter your lastname',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      street: {
        element: 'input',
        value: '',
        config: {
          name: 'street',
          type: 'text',
          placeholder: 'Enter your street',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      house_number: {
        element: 'input',
        value: '',
        config: {
          name: 'house_number',
          type: 'number',
          min: 1,
          placeholder: 'Enter your house number',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      city: {
        element: 'input',
        value: '',
        config: {
          name: 'city',
          type: 'text',
          placeholder: 'Enter your city',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      postal_code: {
        element: 'input',
        value: '',
        config: {
          name: 'postal_code',
          type: 'number',
          min: 1,
          placeholder: 'Enter your postal code',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      province: {
        element: 'input',
        value: '',
        config: {
          name: 'province',
          type: 'text',
          placeholder: 'Enter your province',
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
      country: {
        element: 'select',
        value: '',
        config: {
          name: 'country',
          type: 'select',
          options: [
            { key: 'germany', value: 'Germany' },
            { key: 'austria', value: 'Austria' },
            { key: 'switzerland', value: 'Switzerland' },
          ],
        },
        validation: {
          required: true,
        },
        valid: false,
        validationMessage: '',
        touched: false,
      },
    },
  };

  componentDidUpdate = () => {
    this._formRows(this.state.currentStep);
  };

  isFormValid = (currentStep) => {
    const {
      email,
      password,
      password_confirmation,
      ...rest
    } = this.state.formData;
    let isValid = true;
    switch (currentStep) {
      case 1:
        for (let elem in { email, password, password_confirmation }) {
          isValid = isValid && this.state.formData[elem].valid;
        }
        isValid =
          isValid &&
          this.state.formData.password_confirmation.value ===
            this.state.formData.password.value;
        for (let val in this.state.formData.password.validation) {
          isValid = isValid && this.state.formData.password.validation[val];
        }
        break;
      case 2:
        for (let elem in rest) {
          isValid = isValid && this.state.formData[elem].valid;
        }
        break;
      case 3:
        for (let elem in { email, password, ...rest }) {
          isValid = isValid && this.state.formData[elem].valid;
        }
        break;
      default:
        break;
    }
    this.setState({ formValid: isValid, formError: !isValid });

    return isValid;
  };

  _next = () => {
    let currentStep = this.state.currentStep;
    const isValid = this.isFormValid(currentStep);
    currentStep = currentStep >= 2 ? 3 : currentStep + 1;
    if (isValid) this.navigationButtonHandler(currentStep);
    else {
      this.setState({ formError: true });
    }
  };

  _prev = () => {
    let currentStep = this.state.currentStep;
    currentStep = currentStep <= 1 ? 1 : currentStep - 1;
    this.navigationButtonHandler(currentStep);
  };

  navigationButtonHandler = (currentStep) => {
    this._formRows(currentStep);
    this.showTab(currentStep);
    this.setState({ currentStep });
  };

  tabHandler = (currentStep) => {
    let step = currentStep <= 1 ? 1 : currentStep - 1;
    let isValid = this.isFormValid(step);
    while (step > 1) {
      step--;
      isValid = isValid && this.isFormValid(step);
    }
    if (isValid) this.navigationButtonHandler(currentStep);
    else {
      this.setState({ formError: true });
    }
  };

  showTab = (currentStep) => {
    const buttons = document.getElementsByClassName(`${classes.btn__tab}`);
    let i;
    for (i = 0; i < buttons.length; i++) {
      buttons[i].classList.add(`${classes.btn__tab__inactive}`);
    }
    buttons[currentStep - 1].className = buttons[
      currentStep - 1
    ].className.replace(`${classes.btn__tab__inactive}`, ' ');
  };

  _formRows = (currentStep) => {
    let n;
    if (currentStep === 1) {
      n = 6;
    } else if (currentStep === 2) {
      n = Object.keys(this.state.formData).length - 1;
    } else {
      n = Object.keys(this.state.formData).length + 1;
    }

    if (this.state.formError) n++;

    document.getElementById(
      'wizard-form'
    ).style.gridTemplateRows = `repeat(${n},1fr)`;
  };

  previousButton = () => {
    let currentStep = this.state.currentStep;
    if (currentStep !== 1) {
      return (
        <button
          className={`${classes.btn}`}
          type="button"
          onClick={(event) => this._prev(event)}
        >
          Previous
        </button>
      );
    }
    return null;
  };

  nextButton = () => {
    let currentStep = this.state.currentStep;
    if (currentStep < 3) {
      return (
        <button
          className={`${classes.btn} `}
          type="button"
          onClick={(event) => this._next(event)}
        >
          Next
        </button>
      );
    }
    return null;
  };

  submitButton = () => {
    let currentStep = this.state.currentStep;
    if (currentStep >= 2) {
      return (
        <button
          className={`${classes.btn} ${classes.btnSubmit} `}
          type="submit"
          onClick={(event) => this.formHandler(event)}
        >
          SUBMIT
        </button>
      );
    }
    return null;
  };

  createForm = (formData) => {
    const formElementsArray = [];
    for (let key in formData) {
      formElementsArray.push({
        id: key,
        config: formData[key],
      });
    }

    const formularField = formElementsArray.map((elem) => (
      <React.Fragment key={elem.id}>
        <label htmlFor={elem.id} className={classes.label}>
          {elem.id.replace('_', ' ')}
        </label>
        <FormField
          id={elem.id}
          field={elem.config}
          change={(event) => this.formFieldHandler(event)}
          keyUp={(event) => this.updatePasswordStrength(event)}
          focus={(event) => this.handleFocus(event)}
          blur={(event) => this.handleBlur(event)}
        />
      </React.Fragment>
    ));

    return formularField;
  };

  formFieldHandler = ({ event, id }) => {
    const newFormData = { ...this.state.formData };
    const newElement = { ...newFormData[id] };
    const element = event.target;

    newElement.value = element.value;

    const [valid, validationMessage] = validate(newElement);
    newElement.valid = valid;
    newElement.validationMessage = validationMessage;

    if (element.value === '') {
      element.classList.add(styleForm.empty);
    } else {
      element.classList.remove(styleForm.empty);
    }

    if (id === 'password_confirmation') {
      if (
        element.value !== '' &&
        element.value !== newFormData.password.value
      ) {
        newElement.validationMessage = 'The passwords must be identical';
        newElement.valid = false;
      }
    }

    newFormData[id] = newElement;

    this.setState({
      formData: newFormData,
      formError: false,
    });
  };

  formSuccesManager = (type) => {
    const newFormData = { ...this.state.formData };

    for (let key in newFormData) {
      newFormData[key].value = '';
      newFormData[key].valid = false;
      newFormData[key].validationMessage = '';
    }

    this.clearSuccesMessage();

    this.setState({
      formData: newFormData,
      formError: false,
      formSuccess: type ? 'Congratulation' : 'This user already exists',
    });
  };

  clearSuccesMessage = () => {
    setTimeout(() => {
      this.setState({ formSuccess: '' });
    }, 2000);
  };

  updatePasswordStrength = (event) => {
    const letter = document.getElementById('letter');
    const capital = document.getElementById('capital');
    const number = document.getElementById('number');
    const special = document.getElementById('special');
    const length = document.getElementById('length');
    const bar = document.getElementById('bar');
    let width = 0;
    const newFormData = { ...this.state.formData };
    const password = { ...newFormData.password };
    const validation = { ...password.validation };
    let valid, validationMessage;

    const lowerCaseLetters = /[a-z]/g;
    if (event.target.value.match(lowerCaseLetters)) {
      letter.classList.remove('invalid');
      letter.classList.add('valid');
      width += 10;
      valid = false;
      bar.style.width = width + '%';
      validation.hasLowerCase = true;
    } else {
      letter.classList.remove('valid');
      letter.classList.add('invalid');
      validation.hasLowerCase = false;
    }

    // Validate capital letters
    const upperCaseLetters = /[A-Z]/g;
    if (event.target.value.match(upperCaseLetters)) {
      capital.classList.remove('invalid');
      capital.classList.add('valid');
      width += 20;
      valid = false;
      bar.style.width = width + '%';
      validation.hasUpperCase = true;
    } else {
      capital.classList.remove('valid');
      capital.classList.add('invalid');
      validation.hasUpperCase = false;
    }

    // Validate numbers
    const numbers = /[0-9]/g;
    if (event.target.value.match(numbers)) {
      number.classList.remove('invalid');
      number.classList.add('valid');
      width += 20;
      valid = false;
      bar.style.width = width + '%';
      validation.hasNumber = true;
    } else {
      number.classList.remove('valid');
      number.classList.add('invalid');
      validation.hasNumber = false;
    }

    // Validate special
    const specials = /\W/g;
    if (event.target.value.match(specials)) {
      special.classList.remove('invalid');
      special.classList.add('valid');
      width += 20;
      valid = false;
      bar.style.width = width + '%';
      validation.hasSpecialChar = true;
    } else {
      special.classList.remove('valid');
      special.classList.add('invalid');
      validation.hasSpecialChar = false;
    }

    // Validate length
    if (event.target.value.length >= 8) {
      length.classList.remove('invalid');
      length.classList.add('valid');
      width += 30;
      valid = false;
      bar.style.width = width + '%';
      validation.minLength = true;
    } else {
      length.classList.remove('valid');
      length.classList.add('invalid');
      validation.minLength = false;
    }

    validationMessage =
      width === 100 ? '' : 'Please choose a stronger password';
    valid = width === 100 ? true : false;

    const newPassword = {
      ...password,
      score: width,
      validation,
      valid,
      validationMessage,
    };
    newFormData.password = newPassword;

    this.setState({
      formData: newFormData,
    });
  };

  handleFocus = () => {
    document.getElementById('message').style.display = 'block';
  };

  handleBlur = () => {
    document.getElementById('message').style.display = 'none';
  };

  formHandler = (event) => {
    event.preventDefault();

    const dataToSubmit = {};
    let isValid = true;

    for (let key in this.state.formData) {
      dataToSubmit[key] = this.state.formData[key].value;
      isValid = isValid && this.state.formData[key].valid;
    }

    if (isValid) {
      this.props.onSinup(dataToSubmit);
      this.props.onSetAuthRedirectPath('/sign-in');
      this.props.history.push('/sign-in');
    } else {
      this.setState({ formError: true });
    }
  };

  render() {
    const {
      email,
      password,
      password_confirmation,
      ...rest
    } = this.state.formData;

    const { score, validation } = password;

    const tabs = (
      <div className={`${classes.tab} ${classes.tab__header}`}>
        <button
          className={`${classes.btn} ${classes.btn__tab} `}
          onClick={() => this.tabHandler(1)}
          type="button"
        >
          Main Information
        </button>
        <button
          className={`${classes.btn} ${classes.btn__tab} ${classes.btn__tab__inactive}`}
          onClick={() => this.tabHandler(2)}
          type="button"
        >
          Additional Information
        </button>
        <button
          className={`${classes.btn} ${classes.btn__tab} ${classes.btn__tab__inactive}`}
          onClick={() => this.tabHandler(3)}
          type="button"
        >
          Information Confirmation
        </button>
      </div>
    );

    const form = (
      <form id="wizard-form" className={classes.form}>
        <MainForm
          currentStep={this.state.currentStep}
          formData={{ email, password, password_confirmation }}
          score={score}
          createForm={this.createForm}
        />
        <AdditionalForm
          createForm={this.createForm}
          currentStep={this.state.currentStep}
          formData={rest}
        />
        <ConfirmationForm
          createForm={this.createForm}
          currentStep={this.state.currentStep}
          formData={{
            email,
            password,
            password_confirmation,
            ...rest,
          }}
        />
        <div className={classes.navigation}>
          {this.previousButton()}
          {this.nextButton()}
          {this.submitButton()}
        </div>

        {this.state.formError ? (
          <p className={classes.errorMessage}>
            Please fill all fields properly
          </p>
        ) : null}

        <button
          className={`${classes.btn} ${classes.btnSwitch}`}
          onClick={() => this.props.history.push('/sign-in')}
        >
          SWITCH TO SIGN IN
        </button>
      </form>
    );

    const passwordMessage = (
      <div className="message" id="message">
        <h3>Password must contain the following:</h3>
        <p className="invalid" id="letter">
          {validation.hasLowerCase ? <FaCheck /> : <FaTimes />} A{' '}
          <b>lowercase</b> letter
        </p>
        <p className="invalid" id="capital">
          {validation.hasUpperCase ? <FaCheck /> : <FaTimes />} A{' '}
          <b>capital (uppercase)</b> letter
        </p>
        <p className="invalid" id="number">
          {validation.hasNumber ? <FaCheck /> : <FaTimes />} A <b>number</b>
        </p>
        <p className="invalid" id="special">
          {validation.hasSpecialChar ? <FaCheck /> : <FaTimes />} A{' '}
          <b>special character</b>
        </p>
        <p className="invalid" id="length">
          {validation.minLength ? <FaCheck /> : <FaTimes />} Minimum{' '}
          <b>8 characters</b>
        </p>
      </div>
    );

    return (
      <div className={classes.main}>
        <h2 className={classes.title}>Please SIGN IN</h2>
        <div className={classes.wrapper}>
          {tabs}
          {form}
          {passwordMessage}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  onSetAuthRedirectPath: (path) => dispatch(setAuthRedirectPath(path)),
  onSinup: (credential) => dispatch(signup(credential)),
});

export default connect(null, mapDispatchToProps)(withRouter(SignUp));

import React from 'react';
import Sidebar from '../../components/UI/Sidebar/Sidebar';

import classes from './LayoutAdmin.module.scss';

const Layout = (props) => {
  return (
    <div className={classes.layout_admin}>
      <Sidebar />
      <div className={classes.layout_admin__content}>{props.children}</div>
    </div>
  );
};

export default Layout;

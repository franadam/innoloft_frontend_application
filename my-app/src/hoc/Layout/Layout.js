import React from 'react';
import Header from '../../components/UI/Header/Header';
import Footer from '../../components/UI/Footer/Footer';

import classes from './Layout.module.scss';

const Layout = (props) => {
  return (
    <div className={classes.main}>
      <Header />
      <div className={classes.content}>{props.children}</div>
      <Footer />
    </div>
  );
};

export default Layout;

import React from 'react';
import { connect } from 'react-redux';
import { Switch, Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import './styles/App.scss';

import Layout from './hoc/Layout/Layout';
import SignIn from './components/Authentification/SignIn';
import SignUp from './components/Authentification/SignUp';
import Dashboard from './components/UI/Dashboard/Dashboard';
import Page from './components/UI/Page/Page';

const App = (props) => {
  let routes = (
    <Switch>
      <Route path="/sign-in" component={SignIn} />
      <Route path="/sign-up" component={SignUp} />
      <Redirect to="/sign-in" />
    </Switch>
  );

  if (props.isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/account" render={() => <Page title="account" />} />
        <Route path="/company" render={() => <Page title="company" />} />
        <Route path="/setting" render={() => <Page title="setting" />} />
        <Route path="/news" render={() => <Page title="news" />} />
        <Route path="/analytics" render={() => <Page title="analytics" />} />
        <Redirect to="/dashboard" />
      </Switch>
    );
  }
  return <Layout>{routes}</Layout>;
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: !!state.userID,
  };
};

App.propTypes = {
  isAuthenticated: PropTypes.bool,
};

export default connect(mapStateToProps)(App);

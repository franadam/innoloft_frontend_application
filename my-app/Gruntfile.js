module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-stylelint');
  grunt.loadNpmTasks('grunt-sass-lint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');

  grunt.initConfig({
    eslint: {
      options: {
        configFile: '.eslintrc.json',
        fix: true,
      },
      target: ['src/**/*.js'],
    },
    sasslint: {
      options: {
        configFile: '.sasslintrc.json',
      },
      target: ['src/styles/**/*.scss', 'src/components/**/*.scss'],
    },
    stylelint: {
      options: {
        configFile: '.stylelintrc.json',
        fix: true,
      },
      all: ['src/styles/**/*.scss', 'src/components/**/*.scss'],
    },
    postcss: {
      options: {
        map: false,
        processors: [
          //require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')(), // add vendor prefixes
          //require('cssnano')() // minify the result
        ],
      },
      dist: {
        src: 'src/styles/**/*.scss',
      },
    },
    watch: {
      files: [
        'Gruntfile.js',
        'src/**/*.js',
        'src/components/**/*.scss',
        'src/styles/**/*.scss',
      ],
      tasks: ['stylelint', 'eslint'],
    },
  });

  grunt.registerTask('default', ['eslint', 'postcss']);
};
